SELECT 
flatPageURL as ' URL', 
CAST(flatRoomCount as varchar) || '/' || CAST(flatWholeRoomCount as varchar) || (CASE WHEN flatPictureCount > 0 THEN ' (' || CAST(flatPictureCount as varchar) || ' фото)' ELSE '' END) as 'Кол-во комнат', 
flatAddress as 'Адрес', 
flatNearestSubwayStationNameG || (CASE WHEN flatNearestSubwayStationNameG != flatNearestSubwayStationNameY THEN '/' || flatNearestSubwayStationNameY ELSE '' END) || ' (' || CAST(ROUND(flatNearestSubwayStationDistanceG / 1000, 2) as varchar) || '/' || CAST(ROUND(flatNearestSubwayStationDistanceY / 1000, 2) as varchar) || ' км)' as 'Метро',
CAST(flatFloorNumber as varchar) || flatFloorType || '/' || CAST(flatHouseFloorCount as varchar) || ' (' || flatHouseType || ')' as 'Этаж[тип]/этажность/материал стен',
CAST(flatWholeSquare as varchar) || '/' || CAST(flatLivingSquare as varchar) || '/' || CAST(flatKitchenSquare as varchar) as 'Площадь (общая/жилая/кухня)',
--flatBathroomInfo as 'Сан. узел', 
flatBalconyInfo as 'Балкон', 
--flatBirthInfo as 'Год пастройки', 
--flatSellModeInfo as 'Вид продажи',
CAST(flatPriceInfo as varchar) || ' (' || (CASE WHEN flatPriceDelta > 0 THEN '+' ELSE '' END) || CAST(flatPriceDelta as varchar) || (CASE WHEN isFlatInfoUpdated = 0 THEN ', new' ELSE ', old' END) || ')' as 'Цена',
CAST(ROUND(flatPriceInfo / flatWholeSquare * 1000, 0) as varchar) as 'Цена за метр'
FROM Flats
WHERE
flatRoomCount = 3 AND
--flatHouseType != 'п' AND
(flatFloorNumber = flatHouseFloorCount AND flatHouseFloorCount != 5) AND
flatBathroomInfo != 'совмещенный' AND
(flatBalconyInfo != '' AND flatBalconyInfo != '-') AND
(flatNearestSubwayStationDistanceG <= 2000 or flatNearestSubwayStationDistanceY <= 2000) AND
flatLivingSquare >= 25 AND
--flatPriceInfo <= 100 AND
1=1
ORDER BY flatPriceInfo